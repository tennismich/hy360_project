/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database_hy360;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import static database_hy360.Connection_360.getInitialConnection;

/**
 *
 * @author mike
 */
public class Database_HY360 {

    private static Connection conn;

    public void createTables() throws SQLException {
        Statement stmt = conn.createStatement();

        stmt.execute("CREATE TABLE HY360.User("
            + "type TEXT,"
            + "account_num VARCHAR(14),"
            + "PRIMARY KEY(account_num),"
            + "owed_sum FLOAT,"
            + "name TEXT);");

        stmt.execute("CREATE TABLE HY360.Client ("
            + "account_num VARCHAR(14),"
            + "FOREIGN KEY(account_num)"
            + "REFERENCES User(account_num)"
            + "ON DELETE CASCADE,"
            + "type TEXT, "
            + "credit_limit FLOAT, "
            + "exp_date DATE, "
            + "available_credit FLOAT "
            + ");");

//        stmt.execute("CREATE TABLE HY360.Private_Citizen("
//                + "pc_id INTEGER AUTO_INCREMENT, PRIMARY KEY(pc_id),"
//                +"client_id INTEGER,"
//                +"FOREIGN KEY (client_id) "
//                +"  REFERENCES Client(client_id)"
//                + " ON DELETE CASCADE);");
//
//        stmt.execute("CREATE TABLE HY360.Company("+
//                "c_id INTEGER AUTO_INCREMENT, PRIMARY KEY(c_id),"+
//                "client_id INTEGER,"+
//                "employees TEXT,"
//                +"FOREIGN KEY (client_id) "
//                +"  REFERENCES Client(client_id) "
//                + " ON DELETE CASCADE);");
        stmt.execute("CREATE TABLE HY360.Merchant("
            + "account_num VARCHAR(14),"
            + "FOREIGN KEY(account_num)"
            + "REFERENCES User(account_num)"
            + "ON DELETE CASCADE,"
            + "earnings_sum FLOAT,"
            + "ccc_fee FLOAT"
            + ");");

        stmt.execute("CREATE TABLE HY360.Transaction("
            + "client VARCHAR(14),"
            + "merchant VARCHAR(14),"
            + "trans_id INTEGER AUTO_INCREMENT, PRIMARY KEY(trans_id),"
            + "sum FLOAT,"
            + "date DATE,"
            + "type TEXT,"
            + "FOREIGN KEY (client) "
            + "REFERENCES User(account_num),"
            + "FOREIGN KEY (merchant) "
            + "REFERENCES User(account_num));");

        stmt.execute("CREATE TABLE HY360.Employee("
            + "employee_id INTEGER AUTO_INCREMENT,"
            + "PRIMARY KEY(employee_id),"
            + "full_name TEXT,"
            + "account_num VARCHAR(14),"
            + "FOREIGN KEY(account_num) "
            + "REFERENCES User(account_num) ON DELETE CASCADE"
            + ");");

        stmt.execute("CREATE TABLE HY360.Employee_Transaction("
            + "employee_id INTEGER,"
            + "trans_id INTEGER,"
            + "FOREIGN KEY(employee_id) REFERENCES HY360.Employee(employee_id) ON DELETE CASCADE,"
            + "FOREIGN KEY(trans_id) REFERENCES HY360.Transaction(trans_id));");

    }

    public static void main(String[] args) {

        try {
            Database_HY360 db = new Database_HY360();
            conn = getInitialConnection();
            Statement stmt = conn.createStatement();
            stmt.execute("CREATE DATABASE HY360");

            db.createTables();

            System.out.println("Database created!");
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Database operation failed: " + e.getMessage());
            try {
                Statement stmt = conn.createStatement();
                stmt.execute("DROP DATABASE `HY360`;");
                System.out.println("Database deleted");
            } catch (SQLException e2) {
                System.out.println("Couldnt drop database");
            }
        }
    }

}
