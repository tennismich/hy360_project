/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hy360_client;

/**
 *
 * @author mike
 */
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.*;
import com.google.gson.JsonObject;

public class HY360_CLIENT {

    /**
     * @param args the command line arguments
     */
    private static HttpURLConnection con;
    private static URL postURL;

    public static void getGoodUsers() {
        try {
            URL getURL = new URL("http://localhost:8080/HY360/ccc_servlet?query=getGoodUsers");
            con = (HttpURLConnection) getURL.openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            InputStream is = con.getInputStream();

            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());
            String response = new String(is.readAllBytes());
            System.out.println(response);
        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public static void getBadUsers() {
        try {
            URL getURL = new URL("http://localhost:8080/HY360/ccc_servlet?query=getBadUsers");
            con = (HttpURLConnection) getURL.openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            InputStream is = con.getInputStream();
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());
            String response = new String(is.readAllBytes());
            System.out.println(response);
        } catch (IOException e) {
            System.out.println("File:HY360_CLIENT.java-Exception: " + e.getMessage());
        }
    }

    public static void giveBonus(int month) {
        try {
            URL putURL = new URL("http://localhost:8080/HY360/ccc_servlet?query=giveBonus"
                + "&month_num=" + month);
            con = (HttpURLConnection) putURL.openConnection();
            con.setRequestMethod("PUT");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());
        } catch (MalformedURLException e) {
            System.out.println("File:HY360_CLIENT.java-Exception: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("File:HY360_CLIENT.java-Exception: " + e.getMessage());
        }
    }

    public static void getTransactions(String account_num, String fromDate, String toDate) {
        try {
            URL getURL = new URL(
                "http://localhost:8080/HY360/ccc_servlet?query=getTransactions&account_num=" + account_num
                + "&fromDate=" + fromDate
                + "&toDate=" + toDate);
            con = (HttpURLConnection) getURL.openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            InputStream is = con.getInputStream();

            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());
            String response = new String(is.readAllBytes());
            System.out.println(response);

        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public static void getEmpTransactions(String account_num, int emp) {
        try {
            URL getURL = new URL(
                "http://localhost:8080/HY360/ccc_servlet?query=getEmpTransactions&account_num=" + account_num
                + "&emp_id=" + emp);

            con = (HttpURLConnection) getURL.openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            InputStream is = con.getInputStream();

            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());
            String response = new String(is.readAllBytes());
            System.out.println(response);

        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public static void newTransaction(String client, String merchant, String tranType,
                                      String date, float sum, int emp_id) {
        try {
            URL putURL;
            String url = "http://localhost:8080/HY360/ccc_servlet?query=newTransaction"
                + "&client=" + client
                + "&merchant=" + merchant
                + "&tranType=" + tranType
                + "&sum=" + sum
                + "&date=" + date
                + "&emp_id=" + emp_id;

            putURL = new URL(url);
            con = (HttpURLConnection) putURL.openConnection();
            con.setRequestMethod("PUT");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());
        } catch (IOException e) {

        }
    }

    public static void deleteUser(String account_num) {
        try {
            URL deleteURL;
            deleteURL = new URL(
                "http://localhost:8080/HY360/ccc_servlet?query=payOwed&account_num=" + account_num);

            con = (HttpURLConnection) deleteURL.openConnection();
            con.setRequestMethod("DELETE");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());
        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public static void payOwed(float sum, String account_num) {
        try {
            URL putURL;
            putURL = new URL(
                "http://localhost:8080/HY360/ccc_servlet?query=payOwed&account_num=" + account_num + "&sum=" + sum);

            con = (HttpURLConnection) putURL.openConnection();
            con.setRequestMethod("PUT");

            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());
        } catch (IOException e) {

        }
    }

    public static void newClient(String type, float credit_limit, String exp_date,
                                 String account_num, float available_credit, float owed_sum,
                                 String name) {
        JsonObject newuser = new JsonObject();
        newuser.addProperty("type", type);
        newuser.addProperty("credit_limit", credit_limit);
        newuser.addProperty("exp_date", exp_date);
        newuser.addProperty("account_num", account_num);
        newuser.addProperty("available_credit", available_credit);
        newuser.addProperty("owed_sum", owed_sum);
        newuser.addProperty("name", name);
        String data = newuser.toString();
        try {
            con = (HttpURLConnection) postURL.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            byte[] input = data.getBytes("utf-8");
            os.write(input, 0, input.length);

            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());

        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public static void newEmployee(String account_num, String full_name) {
        JsonObject newuser = new JsonObject();
        newuser.addProperty("type", "employee");
        newuser.addProperty("account_num", account_num);
        newuser.addProperty("full_name", full_name);

        String data = newuser.toString();
        try {
            con = (HttpURLConnection) postURL.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            byte[] input = data.getBytes("utf-8");
            os.write(input, 0, input.length);

            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());

        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public static void newMerchant(float ccc_fee, String account_num, float earnings_sum,
                                   float owed_sum, String name) {
        JsonObject newuser = new JsonObject();
        newuser.addProperty("type", "merchant");
        newuser.addProperty("ccc_fee", ccc_fee);
        newuser.addProperty("account_num", account_num);
        newuser.addProperty("earnings_sum", earnings_sum);
        newuser.addProperty("owed_sum", owed_sum);
        newuser.addProperty("name", name);
        String data = newuser.toString();
        try {
            con = (HttpURLConnection) postURL.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            byte[] input = data.getBytes("utf-8");
            os.write(input, 0, input.length);

            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            System.out.println(con.getResponseCode());

        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public static void fillUpDatabase() {
        newClient("citizen", 2000, "2024/02/13", "00000000000001", 50000, 0,
                  "Micalis Micailoglou");
        newClient("citizen", 2000, "2026/06/23", "00000000000002", 80000, 0,
                  "Giorgos Michailoglou");
        newClient("citizen", 2000, "2027/03/14", "00000000000003", 150000, 0,
                  "Stefanos Michailoglou");

        newClient("citizen", 2000, "2024/02/13", "00000000000001", 50000, 0,
                  "Micalis Micailoglou");
        newClient("citizen", 2000, "2026/06/23", "00000000000002", 80000, 0,
                  "Giorgos Michailoglou");
        newClient("citizen", 2000, "2027/03/14", "00000000000003", 150000, 0,
                  "Stefanos Michailoglou");

        newClient("company", 12000, "2025/01/14", "00000000000004", 354000, 0, "Eteria_1");
        newClient("company", 122000, "2022/11/14", "00000000000005", 1354000, 0, "Eteria_2");
        newMerchant(20, "00000000000006", 0, 0, "Emporos Emporoglou");
        newMerchant(10, "00000000000007", 0, 0, "Giorgos Georgiou");
        newMerchant(20, "00000000000008", 0, 0, "Xrhstos Xrhstou");
        newMerchant(30, "00000000000009", 0, 0, "Filipos FIlipou");
        newMerchant(40, "00000000000010", 0, 0, "Paris Filipou");
        newEmployee("00000000000005", "Kostas Kostopoulos");
        newEmployee("00000000000005", "Charitos Charitoglou");
        newEmployee("00000000000004", "Anastashs Kostopoulos");
        newEmployee("00000000000004", "Kwstas Anastasopoulos");
        newEmployee("00000000000004", "Yanis Yanoglou");
        newEmployee("00000000000004", "Michalis Yanoglou");
    }

    public static void performActions() {
        newTransaction("00000000000001", "00000000000007", "sell", "2022/12/12", 500, -1);
        newTransaction("00000000000003", "00000000000009", "sell", "2022/09/02", 1100, -1);

        newTransaction("00000000000005", "00000000000010", "sell", "2022/05/03", 1410, 2);
        newTransaction("00000000000005", "00000000000007", "sell", "2022/03/01", 2100, 1);
        newTransaction("00000000000003", "00000000000009", "sell", "2022/09/02", 100, 1);

        newTransaction("00000000000004", "00000000000007", "sell", "2022/02/11", 100, 5);
        newTransaction("00000000000003", "00000000000009", "sell", "2022/09/12", 500, 1);
        newTransaction("00000000000003", "00000000000009", "return", "2022/01/22", 500, 1);

    }

    public static void getStuffFromDatabase() {
        System.out.println("Users who owed money to the company");

    }

    public static void main(String[] args) {
        try {
            postURL = new URL("http://localhost:8080/HY360/ccc_servlet");

            // fillUpDatabase();
            // performActions();
            getGoodUsers();
            getBadUsers();
            giveBonus(3);
        } catch (MalformedURLException ex) {
            Logger.getLogger(HY360_CLIENT.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
