/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import database.*;
import ccc_classes.*;
import java.util.ArrayList;

/**
 *
 * @author mike
 */
public class ccc_servlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String type = request.getParameter("query");
        CommonQueries cq = new CommonQueries();
        Gson gson = new Gson();
        PrintWriter out = response.getWriter();
        switch (type) {
            case "getTransactions":
                ArrayList<Transaction> transArr;
                transArr = cq.getTransactions(request.getParameter("account_num"),
                                              request.getParameter("fromDate"),
                                              request.getParameter("toDate"));
                String json = gson.toJson(transArr);
                response.setContentType("application/json");
                response.setStatus(200);
                out.print(json);
                break;
            case "getEmpTransactions":
                ArrayList<Transaction> transArr2;
                transArr2 = cq.getEmployeeTransactions(request.getParameter("account_num"),
                                                       Integer.parseInt(request.getParameter(
                                                           "emp_id")));
                String json2 = gson.toJson(transArr2);

                response.setContentType("application/json");
                response.setStatus(200);
                out.print(json2);
                break;
            case "getGoodUsers":
                ArrayList<User> users;
                users = cq.getGoodUsers();

                String json3 = gson.toJson(users);
                response.setContentType("application/json");
                response.setStatus(200);
                out.print(json3);
                break;
            case "getBadUsers":
                ArrayList<User> users2;
                users2 = cq.getBadUsers();
                String json4 = gson.toJson(users2);
                response.setContentType("application/json");
                response.setStatus(200);
                out.print(json4);
                break;
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        EditUserTable et = new EditUserTable();
        if (et.deleteAccount(request.getParameter("account_num")) == true) {
            response.setStatus(200);
        } else {
            response.setStatus(409);
        }

    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String type = request.getParameter("query");
        EditUserTable et = new EditUserTable();
        EditTransactionTable etr = new EditTransactionTable();
        CommonQueries cq = new CommonQueries();
        switch (type) {
            case "payOwed":
                et.payOwedSum(Float.parseFloat(request.getParameter("sum")),
                              request.getParameter("account_num"));
                response.setStatus(200);
                break;
            case "newTransaction":
                Transaction newtran
                            = new Transaction(request.getParameter("client"),
                                              request.getParameter("merchant"),
                                              request.getParameter("tranType"),
                                              Float.parseFloat(request.getParameter("sum")),
                                              request.getParameter("date"));

                if (etr.newTransaction(newtran, Integer.parseInt(request.getParameter(
                                       "emp_id")))) {
                    response.setStatus(200);
                } else {
                    response.setStatus(422);
                }
                break;
            case "giveBonus":
                String res = cq.giveBonus(Integer.parseInt(request.getParameter("month_num")));
                PrintWriter out = response.getWriter();
                out.print(res);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String data = request.getReader().lines().collect(Collectors.joining(
            System.lineSeparator()));
        Gson gson = new Gson();

        JsonObject jsonObj = gson.fromJson(data, JsonObject.class);

        System.out.println(data);
        EditClientTables ec = new EditClientTables();
        switch (jsonObj.get("type").toString()) {
            case "\"citizen\"":
                ec.addClient(gson.fromJson(data, Client.class));
                break;
            case "\"company\"":
                ec.addClient(gson.fromJson(data, Client.class));

                break;
            case "\"merchant\"":
                EditMerchantTable em = new EditMerchantTable();
                em.addMerchant(gson.fromJson(data, Merchant.class));
                break;
            case "\"employee\"":
                EditEmployeeTable ee = new EditEmployeeTable();
                ee.addEmployee(gson.fromJson(data, Employee.class));
                break;

            default:
                response.setStatus(400);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
