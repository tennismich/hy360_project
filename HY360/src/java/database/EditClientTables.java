/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import ccc_classes.*;
import java.sql.Connection;
import java.sql.Statement;

import java.sql.SQLException;

/**
 *
 * @author mike
 */
public class EditClientTables {

    public void addClient(Client c) {
        try {
            Connection con = Connection_360.getInitialConnection();
            Statement s = con.createStatement();
            s.execute("INSERT INTO HY360.User (type, account_num, owed_sum, name) VALUES(" + "'" + c.getType() + "','" + c.getAccount_num() + "'," + c.getOwed_sum() + ",'" + c.getName() + "');");

            s.execute("INSERT INTO HY360.Client (account_num, type, credit_limit,exp_date, available_credit) VALUES('" + c.getAccount_num() + "','" + c.getType() + "'," + c.getCredit_limit() + ",STR_TO_DATE('" + c.getExp_date() + "', '%Y/%m/%d')," + c.getAvailable_credit() + ");");

//s.execute("INSERT INTO HY360.Company (client_id, name, employees) VALUES(LAST_INSERT_ID(),'"+c.getName()+"',"+c.getE_list() +");");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Exception thown: " + e.getMessage());
        }
    }

    public void removeClient(int account_num) {
        try {
            Connection con = Connection_360.getInitialConnection();
            Statement s = con.createStatement();
            s.execute("DELETE FROM HY360.Client WHERE account_num=" + account_num + ";");

            //System.out.println(c.getExp_date());
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Exception thown: " + e.getMessage());
        }
    }

    public void newPayment(int account_num, float sum) {
        try {
            Connection con = Connection_360.getInitialConnection();
            Statement s = con.createStatement();
            s.execute("UPDATE HY360.Client SET owed_sum=owed_sum-" + sum + " WHERE account_num=" + account_num + ";");

            //System.out.println(c.getExp_date());
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Exception thown: " + e.getMessage());
        }
    }

}
