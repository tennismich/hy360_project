/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import ccc_classes.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 *
 * @author mike
 */
public class EditTransactionTable {

    public boolean newTransaction(Transaction t, int emp_id) {
        String merchantName, clientName;
        CommonQueries cq = new CommonQueries();
        ResultSet merchantRes, clientRes, clientRes2;
        float client_sum, client_limit, client_owed;
        float ccc_fee;

        try {
            ccc_fee = cq.getMerchant(t.getMerchant()).getFloat("ccc_fee");
            merchantRes = cq.getUser(t.getMerchant());
            clientRes = cq.getUser(t.getClient());
            if (clientRes != null && merchantRes != null) {
                merchantName = merchantRes.getString("account_num");

                clientName = clientRes.getString("account_num");
                clientRes2 = cq.getClient(t.getClient());
                client_sum = clientRes2.getFloat("available_credit");
                client_limit = clientRes2.getFloat("credit_limit");
                client_owed = clientRes.getFloat("owed_sum");
                Connection con = Connection_360.getInitialConnection();
                Statement s = con.createStatement();

                switch (t.getType()) {
                    case "sell":

                        if (client_limit < client_owed || client_sum < t.getSum()) {
                            return false;
                        }

                        s.execute("UPDATE HY360.User SET owed_sum=owed_sum+" + t.getSum()
                            + "WHERE account_num='" + t.getClient() + "'");

                        s.execute("UPDATE HY360.Client SET available_credit=available_credit-" + t.getSum()
                            + "WHERE account_num='" + t.getClient() + "'");

                        s.execute("UPDATE HY360.Merchant SET earnings_sum=earnings_sum+" + t.getSum()
                            + "WHERE "
                            + "account_num='" + t.getMerchant() + "'");

                        s.execute("UPDATE HY360.User SET owed_sum=owed_sum+(" + ccc_fee
                            + "/100)*" + t.getSum()
                            + " WHERE account_num='" + t.getMerchant() + "'");

                        break;

                    case "return":
                        s.execute("UPDATE HY360.User SET owed_sum=owed_sum-" + t.getSum()
                            + "WHERE account_num='" + t.getClient() + "'");

                        s.execute("UPDATE HY360.Client SET available_credit=available_credit+" + t.getSum()
                            + "WHERE account_num='" + t.getClient() + "'");

                        s.execute("UPDATE HY360.Merchant SET earnings_sum=earnings_sum-" + t.getSum()
                            + " WHERE account_num='" + t.getMerchant() + "'");

                        s.execute("UPDATE HY360.User SET owed_sum=owed_sum-(" + ccc_fee
                            + "/100)*" + t.getSum()
                            + " WHERE account_num='" + t.getMerchant() + "'");

                        break;
                    default:
                        return false;
                }

                s.execute("INSERT INTO HY360.Transaction (client, merchant, sum, date, type) "
                    + "VALUES('" + clientName + "','" + merchantName + "'," + t.getSum() + ","
                    + "STR_TO_DATE('" + t.getDate() + "', '%Y/%m/%d')," + "'" + t.getType() + "');");

                if (emp_id != -1) { // Einai ypallhlos etairias

                    s.execute("INSERT INTO HY360.employee_transaction (employee_id, trans_id)"
                        + "VALUES( " + emp_id + ", LAST_INSERT_ID());");
                }

                return true;
            }

        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Exception thown: " + e.getMessage());
            return false;
        }
        return false;
    }

}
