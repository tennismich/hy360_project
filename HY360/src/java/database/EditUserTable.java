/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package database;
import ccc_classes.*;
import com.google.gson.*;
import com.mysql.jdbc.*;
import database.Connection_360;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import database.CommonQueries;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mike
 */

interface callback{
   
}

public class EditUserTable {
    
    public void payOwedSum(float sum, String account_num){
        try{
            Connection con=Connection_360.getInitialConnection();
            Statement s=con.createStatement();
            s.execute("UPDATE HY360.User SET owed_sum=owed_sum-"+sum+" WHERE account_num='"+account_num+"'");
            
        }
        catch(ClassNotFoundException| SQLException e ){
            System.out.println("Exception thown: "+e.getMessage());
        }
    }
    
    public boolean deleteAccount(String account_num){
        try{
            CommonQueries q=new CommonQueries();
           
            Connection con=Connection_360.getInitialConnection();
            Statement s=con.createStatement();
            s.execute("DELETE FROM HY360.User WHERE account_num='"+account_num+"' AND owed_sum=0");
            if( q.getUser(account_num)==null){
                return true;
            }
            
        }
        catch(ClassNotFoundException| SQLException e ){
            System.out.println("Exception thown: "+e.getMessage());
        }
        return false;
    }
    

}
