/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import ccc_classes.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.SQLException;

/**
 *
 * @author mike
 */
public class CommonQueries {

    public ResultSet getUser(String account_num) {

        try {
            ResultSet rs;
            Connection con = Connection_360.getInitialConnection();
            Statement stmt = con.createStatement();
            rs = stmt.executeQuery(
                "SELECT * FROM HY360.User WHERE account_num='" + account_num + "'");
            if (rs.next() == false) {
                return null;
            }
            return rs;

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        return null;
    }

    public ResultSet getClient(String account_num) {
        try {
            ResultSet rs;
            Connection con = Connection_360.getInitialConnection();
            Statement stmt = con.createStatement();
            rs = stmt.executeQuery(
                "SELECT * FROM HY360.Client WHERE account_num='" + account_num + "'");
            if (rs.next() == false) {
                return null;
            }
            return rs;

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        return null;
    }

    public ResultSet getMerchant(String account_num) {
        try {
            ResultSet rs;
            Connection con = Connection_360.getInitialConnection();
            Statement stmt = con.createStatement();
            rs = stmt
                .executeQuery(
                    "SELECT * FROM HY360.merchant WHERE account_num='" + account_num + "'");
            if (rs.next() == false) {
                return null;
            }
            return rs;

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<Transaction> getTransactions(String account_num, String fromDate,
                                                  String toDate) {
        try {
            ResultSet rs;
            Connection con = Connection_360.getInitialConnection();
            Statement stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM HY360.transaction WHERE (client='"
                + account_num + "' OR merchant='" + account_num + "')AND date>='" + fromDate
                + "' AND date<='" + toDate + "'");
            ArrayList<Transaction> transArr = new ArrayList<Transaction>();
            while (rs.next()) {
                transArr.add(new Transaction(rs.getString("client"),
                                             rs.getString("merchant"),
                                             rs.getString("type"),
                                             rs.getFloat("sum"),
                                             rs.getString("date")));
                System.out.print(rs.getString("client"));
            }
            return transArr;

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<Transaction> getEmployeeTransactions(String account_num, int emp) {
        try {
            ResultSet rs;
            Connection con = Connection_360.getInitialConnection();
            Statement stmt = con.createStatement();
            if (emp == -1) {
                rs = stmt.executeQuery(
                    "SELECT * FROM HY360.transaction JOIN HY360.employee_transaction WHERE "
                    + "(HY360.transaction.trans_id=HY360.employee_transaction.trans_id) "
                    + "AND HY360.transaction.client=" + account_num);
            } else {
                rs = stmt.executeQuery(
                    "SELECT * FROM HY360.transaction JOIN HY360.employee_transaction WHERE "
                    + "(HY360.transaction.trans_id=HY360.employee_transaction.trans_id) "
                    + "AND HY360.employee_transaction.employee_id=" + emp);
            }
            ArrayList<Transaction> transArr = new ArrayList<Transaction>();
            while (rs.next()) {
                transArr.add(new Transaction(rs.getString("client"),
                                             rs.getString("merchant"),
                                             rs.getString("type"),
                                             rs.getFloat("sum"),
                                             rs.getString("date")));
            }
            return transArr;

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<User> getGoodUsers() {
        try {
            ResultSet rs;
            Connection con = Connection_360.getInitialConnection();
            Statement stmt = con.createStatement();

            ArrayList<User> usersArr = new ArrayList<User>();

            rs = stmt.executeQuery("SELECT * FROM HY360.user WHERE owed_sum=0");

            while (rs.next()) {
                usersArr.add(new User(rs.getString("account_num"),
                                      Float.parseFloat(rs.getString("owed_sum")),
                                      rs.getString("name")));
            }
            return usersArr;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

        return null;
    }

    public ArrayList<User> getBadUsers() {
        try {
            ResultSet rs;
            Connection con = Connection_360.getInitialConnection();
            Statement stmt = con.createStatement();

            ArrayList<User> usersArr = new ArrayList<User>();

            rs = stmt.executeQuery("SELECT * FROM HY360.user WHERE"
                + " owed_sum>0"
                + " ORDER BY owed_sum DESC");

            while (rs.next()) {
                usersArr.add(new User(rs.getString("account_num"),
                                      Float.parseFloat(rs.getString("owed_sum")),
                                      rs.getString("name")));
            }
            return usersArr;
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

        return null;
    }

    public String giveBonus(int month_number) {
        try {
            ResultSet rs;
            Connection con = Connection_360.getInitialConnection();
            Statement stmt = con.createStatement();

            rs = stmt.executeQuery("SELECT COUNT(merchant), merchant"
                + "FROM transaction"
                + "WHERE MONTH(date)=" + month_number
                + "GROUP BY merchant LIMIT 1");

            String winner_num = rs.getString("merchant");

            stmt.executeQuery(
                "UPDATE HY360.merchant SET ccc_fee=ccc_fee-0.05*ccc_fee WHERE account_num='" + winner_num + "'");

            if (rs.next() == false) {
                return null;
            }
            return winner_num;

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        return null;
    }
}
