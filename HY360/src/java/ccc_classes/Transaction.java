/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccc_classes;
import ccc_classes.*;
/**
 *
 * @author mike
 */
public class Transaction {
    String client;
    String merchant;
    String type,date;
    float sum;

    public String getClient() {
        return client;
    }

    public String getMerchant() {
        return merchant;
    }

    public String getType() {
        return type;
    }

    public float getSum() {
        return sum;
    }

    public String getDate() {
        return date;
    }
    

    public Transaction(String client, String merchant, String type, float sum, String date) {
        this.client = client;
        this.merchant = merchant;
        this.type = type;
        this.sum = sum;
        this.date=date;
    }
    
}
