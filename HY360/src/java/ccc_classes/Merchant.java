/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccc_classes;

/**
 *
 * @author mike
 */
public class Merchant extends User {

    protected float earnings_sum, ccc_fee;

    public float getEarnings_sum() {
        return earnings_sum;
    }

    public void setEarnings_sum(float earnings_sum) {
        this.earnings_sum = earnings_sum;
    }

    public float getCcc_fee() {
        return ccc_fee;
    }

    public void setCcc_fee(float ccc_fee) {
        this.ccc_fee = ccc_fee;
    }

    public String getName() {
        return super.name;
    }

    public float getOwed_sum() {
        return super.owed_sum;
    }

    public String getAccount_num() {
        return super.account_num;
    }

    public Merchant(float earnings_sum, float ccc_fee, String account_num, float owed_sum,
                    String name) {
        super(account_num, owed_sum, name);
        this.earnings_sum = earnings_sum;
        this.ccc_fee = ccc_fee;
    }

}
