/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccc_classes;

/**
 *
 * @author mike
 */
public class Employee {

    private String full_name;
    private String account_num;

    public String getFull_name() {
        return full_name;
    }

    public String getAccount_num() {
        return account_num;
    }

    public Employee(String full_name, String account_num) {
        this.full_name = full_name;

        this.account_num = account_num;
    }

}
