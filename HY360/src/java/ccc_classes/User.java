/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccc_classes;

/**
 *
 * @author mike
 */
public class User {

    protected String account_num;
    protected float owed_sum;
    protected String name;

    public User(String account_num, float owed_sum, String name) {
        this.account_num = account_num;
        this.owed_sum = owed_sum;
        this.name = name;
    }
}
