/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccc_classes;

import java.util.Date;

/**
 *
 * @author mike
 */
public class Client extends User {

    protected float credit_limit, available_credit;
    protected String exp_date, type;

    public float getCredit_limit() {
        return credit_limit;
    }

    public float getOwed_sum() {
        return owed_sum;
    }

    public float getAvailable_credit() {
        return available_credit;
    }

    public String getExp_date() {
        return exp_date;
    }

    public String getType() {
        return type;
    }

    public String getAccount_num() {
        return super.account_num;
    }

    public String getName() {
        return super.name;
    }

    public Client(float credit_limit, float available_credit, String exp_date, String type,
                  String account_num, float owed_sum, String name) {
        super(account_num, owed_sum, name);
        this.credit_limit = credit_limit;
        this.owed_sum = owed_sum;
        this.available_credit = available_credit;
        this.exp_date = exp_date;
        this.type = type;
    }
}
